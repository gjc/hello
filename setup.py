from setuptools import setup

setup(name='hello',
      version='0.1.0',
      description='Can you guess',
      url='https://bitbusket.org/gjc/hello',
      author='Gavin',
      author_email='',
      license='MIT',
      packages=['hello'],
      zip_safe=False)
